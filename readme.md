#### Servlet部署

- 打包文件 `mvn clean package`

- 复制文件夹目录的文件 `target/appdemo-0.0.1-SNAPSHOT/` 到 tomcat `webapps/appdemo` 里面

- 访问: http://localhost:8080/appdemo/ts 这里的`ts`是项目了里面设置的`url-pattern`

- 记得重启tomcat`/Volumes/exfat/apache-tomcat-9.0.39/bin/shutdown.sh`

- webapps 目录

```shell
jcleng@192  /Volumes/exfat/apache-tomcat-9.0.39/webapps  tree . -L 2
.
├── ROOT
│   ├── WEB-INF
│   ├── ...
├── manager
│   ├── ...
│   ├── ...
└── appdemo
    ├── META-INF
    │   └── MANIFEST.MF
    └── WEB-INF
        ├── classes
        │    └── web
        │         └── Main.class
        └── web.xml
```

- 开发阶段

- 热部署 参考: https://www.cnblogs.com/hongdada/p/10123128.html

- 1 编辑tomcat配置文件,访问manager服务,文件`/Volumes/exfat/apache-tomcat-9.0.39/conf/tomcat-users.xml`,取消注释

```xml
<role rolename="admin"/>
<role rolename="manager-script"/>
<role rolename="manager-gui"/>
<role rolename="manager-jmx"/>
<role rolename="manager-status"/>
<role rolename="admin-gui"/>
<user username="admin" password="admin" roles="manager-gui,admin,manager-jmx,manager-script,manager-status,admin-gui" />
```

- 2 编辑`pom.xml`

```xml
<plugin>
    <groupId>org.apache.tomcat.maven</groupId>
    <artifactId>tomcat7-maven-plugin</artifactId>
    <version>2.2</version>
    <configuration>
        <path>/${project.artifactId}</path>
        <url>http://localhost:8080/manager/text</url>
        <!-- 这个是maven的id -->
        <server>tomcat</server>
        <!-- tomcat-user.xml 的 username -->
        <username>admin</username>
        <!-- tomcat-user,xml 的 password -->
        <password>admin</password>
        <update>true</update>
    </configuration>
</plugin>
```

- 3 maven文件修改 /Volumes/exfat/apache-maven-3.6.3/conf/settings.xml 这个是配置的名称和密码

```xml
<server>
    <id>tomcat</id>
    <username>admin</username>
    <password>admin</password>
</server>
```

- 执行 `mvn clean tomcat7:deploy`
- `mvn tomcat7:run`
```shell
tomcat7:deploy --部署web war包
tomcat7:redeploy --重新部署web war包
tomcat7:undeploy --停止该项目运行，并删除部署的war包
tomcat7:run --启动嵌入式tomcat ，并运行当前项目
tomcat7:exec-war --创建一个可执行的jar文件，允许使用java -jar mywebapp.jar 运行web项目
tomcat7:help --在tomcat7-maven-plugin显示帮助信息
```
